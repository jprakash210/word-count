package wordcounter;

import com.exercise.exception.InvalidInputException;
import com.exercise.translator.Translator;
import com.exercise.wordcounter.WordCounter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class WordCounterTest  
{

    private WordCounter wordCounter;

    private Translator translator;

    @Before
    public void setItUp() {
        translator = mock(Translator.class);
        wordCounter = new WordCounter(translator);
    }
	
	@Test
	public void testAddOneWord() throws InvalidInputException {
		wordCounter.addWord("one");
		assertTrue(wordCounter.getWordList().size() == 1);
	}
	
	@Test
	public void testAddMoreThanOneWord() throws InvalidInputException {
		wordCounter.addWord("one");
		wordCounter.addWord("two");
		wordCounter.addWord("three");
		assertTrue(wordCounter.getWordList().size() == 3);
	}
	
	@Test(expected = InvalidInputException.class)
	public void testAddMoreThanOneWordInString() throws InvalidInputException {
		wordCounter.addWord("one two");
	}
	
	@Test(expected = InvalidInputException.class)
	public void testAddWordWithNonAlphabeticCharacter() throws InvalidInputException {
		wordCounter.addWord("one123"); 
	}
    
	@Test
	public void getWordCountTest() throws InvalidInputException {
		when(translator.translate(anyString())).then(returnsFirstArg());
		wordCounter.addWord("flower");
		wordCounter.addWord("vegetable");
		assertTrue(wordCounter.getWordCount("flower")== 1);
	}
	
	@Test
	public void getWordCountForDifferentLangTest() throws InvalidInputException {
		when(translator.translate("flower")).thenReturn("flower");
		when(translator.translate("flor")).thenReturn("flower");
		when(translator.translate("blume")).thenReturn("flower");
		when(translator.translate("vegetable")).thenReturn("vegetable");
		wordCounter.addWord("flower");
		wordCounter.addWord("flor");
		wordCounter.addWord("blume");
		wordCounter.addWord("vegetable");
		assertTrue(wordCounter.getWordCount("Flower")== 3);
	}	
}
