package com.exercise.wordcounter;

import com.exercise.exception.InvalidInputException;
import com.exercise.translator.Translator;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Word Counter - To add words and count the particular word
 *
 */
public class WordCounter 
{
	private Translator translator;
	private List<String> wordList ;
	private static final String alphaPattern = "^[a-zA-Z]*$";
	
    public WordCounter(Translator translator) {
        this.wordList = new ArrayList<>();
        this.translator = translator;
    }
    
    public List<String> getWordList(){
    	return this.wordList;
    }
    
    /*
     * Input String
     * returns void
     * throws InvalidInputException if validation of input fails
     * if the word contains only alphabets it will add to the list of words
     */

	
	public void addWord(String word) throws InvalidInputException{
		if(validateWord(word)) {
			this.wordList.add(word);
		}
	}
	
	/*
	 * Input - String
	 * returns the count of the given word 
	 */
	
	public long getWordCount(String word) {
		long wordCount = 0;
		wordCount = this.wordList
						  .stream()
						  .map(translateWord)
						  .filter(i -> i.equals(word.toLowerCase())).count();
		return wordCount;
	}
	
	
	private boolean validateWord(String word) throws InvalidInputException{
		if(word != null && word != "" && word.matches(alphaPattern)) 
			return true;
		else
			throw new InvalidInputException("The words should not contain non-alphabetic characters");
	}
	
    public Function<String,String> translateWord = 
   	(word)-> {
   		String translatedWord = this.translator.translate(word);
   		return translatedWord.toLowerCase();
   };
}
